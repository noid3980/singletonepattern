using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SingletonPattern
{
    public class GameController : MonoBehaviour
    {
        public void TestCharpSingleTon()
        {
            Debug.Log("C#");

            SingletonCSharp instnance = SingletonCSharp.Instance;
            instnance.TestSinglton();

            SingletonCSharp instance2 = SingletonCSharp.Instance;
            instance2.TestSinglton();
        }

        public void TesUnitySingleTon()
        {
            Debug.Log("Unity");

            SingletonUnity instance = SingletonUnity.Instance;
            instance.TestSingleton();

            SingletonUnity instance2 = SingletonUnity.Instance;
            instance2.TestSingleton();
        }
    } 
}
